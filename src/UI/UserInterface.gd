class_name UI
extends CanvasLayer


func _ready():
	InputMap.erase_action("example-btn")
	InputMap.add_action("example-btn")
	print("UseInterface initialized")


func _physics_process(_delta):

	if Input.is_action_just_pressed("example-btn"):
		var node = TouchScreenButton.new()
		node.set_position(Vector2(rand_range(10, 400), rand_range(10, 900)))
		
		node.set_action("example-btn")
		node.set_scale(Vector2(25, 25))
		
		
		var texture = $TouchScreenButton.get_texture()
		node.set_texture(texture)
		print(texture)
		var parent = $Swipe
		parent.add_child(node) 
		print("example btn pressed")
		print(get_tree().get_node_count())
