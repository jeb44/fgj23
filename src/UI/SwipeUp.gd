extends Node


const SENSITIVITY_SLIDE := 10

#This wont be called if a GUI or something else is handling the event
func _unhandled_input(event):
	if event is InputEventScreenDrag:
		
		# The faster the swipe, the higher the relative value
		var swipe = event.relative
		if swipe.y > SENSITIVITY_SLIDE : print("up")
		elif swipe.y < -SENSITIVITY_SLIDE : print("down")
		elif swipe.x < -SENSITIVITY_SLIDE: 
			if get_tree().get_current_scene().get_name() != "Merkkaus":
				get_tree().change_scene("res://src/Merkkaus/Merkkaus.tscn")
		elif swipe.x > SENSITIVITY_SLIDE:
			if get_tree().get_current_scene().get_name() != "Main":
				get_tree().change_scene("res://src/Main/main.tscn")

#Also enable "emulate touch from mouse" to test from pc
